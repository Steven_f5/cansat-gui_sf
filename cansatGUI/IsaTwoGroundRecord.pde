public static class IsaTwoGroundRecord {
  public float roll, yaw, pitch, pressure, temperature, altitude, co2, xPos, yPos, zPos, latitude, longitude, TL_lat, TL_long, TR_lat, TR_long, BR_lat, BR_long, BL_lat, BL_long, altitude_GPS, altitude_GPScorr;
  public int timestampTrim;
  public float caX, caY, caZ, cgX, cgY, cgZ, cmX, cmY, cmZ;
  public float raX, raY, raZ, rgX, rgY, rgZ, rmX, rmY, rmZ;
  public float fix;

  public IsaTwoGroundRecord(String line) {
    if (line != null) {
      String[] data = line.split(",");

      String timestamp;
      timestamp = trim(data[1]);
      timestampTrim = Integer.parseInt(timestamp);

      fix=Float.parseFloat(data[11]);

      roll = Float.parseFloat(data[32]);
      yaw = Float.parseFloat(data[33]);
      pitch = Float.parseFloat(data[34]);

      pressure = Float.parseFloat(data[16]);
      temperature = Float.parseFloat(data[47]);
      altitude = Float.parseFloat(data[17]);
      altitude_GPS = Float.parseFloat(data[14]);
      altitude_GPScorr = Float.parseFloat(data[48]);
      co2 = Float.parseFloat(data[50]);

      xPos = Float.parseFloat(data[38]);
      yPos = Float.parseFloat(data[39]);
      zPos = Float.parseFloat(data[40]);

      latitude = Float.parseFloat(data[12]);
      longitude = Float.parseFloat(data[13]);

      TL_lat = Float.parseFloat(data[51]);
      TL_long = Float.parseFloat(data[55]);
      TR_lat = Float.parseFloat(data[52]);
      TR_long = Float.parseFloat(data[56]);
      BR_lat = Float.parseFloat(data[54]);
      BR_long = Float.parseFloat(data[58]);
      BL_lat = Float.parseFloat(data[53]);
      BL_long = Float.parseFloat(data[57]);

      caX=Float.parseFloat(data[20]);
      caY=Float.parseFloat(data[21]);
      caZ=Float.parseFloat(data[22]);
      cgX=Float.parseFloat(data[23]);
      cgY=Float.parseFloat(data[24]);
      cgZ=Float.parseFloat(data[25]);
      cmX=Float.parseFloat(data[26]);
      cmY=Float.parseFloat(data[27]);
      cmZ=Float.parseFloat(data[28]);

      raX=Float.parseFloat(data[2]);
      raY=Float.parseFloat(data[3]);
      raZ=Float.parseFloat(data[4]);
      rgX=Float.parseFloat(data[5]);
      rgY=Float.parseFloat(data[6]);
      rgZ=Float.parseFloat(data[7]);
      rmX=Float.parseFloat(data[8]);
      rmY=Float.parseFloat(data[9]);
      rmZ=Float.parseFloat(data[10]);
    }
  }
}
